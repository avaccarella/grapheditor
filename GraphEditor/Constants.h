//
//  Constants.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 29/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#ifndef GraphEditor_Constants_h
#define GraphEditor_Constants_h


#define NEW_DATA_NOTIFICATION           @"JsonDataNotificationName"
#define NETWORK_ERROR_NOTIFICATION      @"NetworkErrorNotificationName"

#define DELETED_NODE_NOTIFICATION       @"DeletedNodeNotificationName"
#define EXPLORE_FROM_NODE_NOTIFICATION  @"ExploreFromNodeNotificationName"
#define EDITED_NODE_NOTIFICATION        @"NodeEditedNotificationName"

#define NODE_INDEX_KEY                  @"nodeidxkey"
#define NODE_X_KEY                      @"x"
#define NODE_Y_KEY                      @"y"
#define NODE_R_KEY                      @"r"
#define NODE_L_KEY                      @"l"
#define DATA_FROM_NETWORK_KEY           @"data"
#endif
