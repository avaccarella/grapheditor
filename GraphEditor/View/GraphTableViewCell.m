//
//  GraphTableViewCell.m
//  GraphEditor
//
//  Created by Alberto Vaccarella on 29/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import "GraphTableViewCell.h"
#import "Constants.h"

@implementation GraphTableViewCell

@synthesize node = _node;

-(void)setNode:(GraphNode *)node {
    _node = node;
    [self.xValue setText:[NSString stringWithFormat:@"%lf", _node.x]];
    [self.yValue setText:[NSString stringWithFormat:@"%lf", _node.y]];
    [self.rValue setText:[NSString stringWithFormat:@"%lf", _node.r]];
    NSArray* adjArr = [_node getAdjacentNodesIndexes];
    NSString* adjStr = [adjArr componentsJoinedByString:@", "];
    [self.adjacentNodesLabel setText:adjStr];
    [self.nodeIndexLabel setText:[NSString stringWithFormat:@"N%@", _node.index]];
}

- (IBAction)startExploration:(id)sender {
    NSDictionary* info = [NSDictionary dictionaryWithObject:self.node.index forKey:NODE_INDEX_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:EXPLORE_FROM_NODE_NOTIFICATION object:nil userInfo:info];
}

- (IBAction)xEdited:(UITextField *)sender {
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    [_node setX:[[f numberFromString:self.xValue.text] doubleValue]];

    NSDictionary* info = [NSDictionary dictionaryWithObject:self.node.index forKey:NODE_INDEX_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:EDITED_NODE_NOTIFICATION object:nil userInfo:info];
}

- (IBAction)yEdited:(UITextField *)sender {
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    [_node setY:[[f numberFromString:self.yValue.text] doubleValue]];
    
    NSDictionary* info = [NSDictionary dictionaryWithObject:self.node.index forKey:NODE_INDEX_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:EDITED_NODE_NOTIFICATION object:nil userInfo:info];
}

- (IBAction)radiusEdited:(UITextField *)sender {
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    [_node setR:[[f numberFromString:self.rValue.text] doubleValue]];
    
    NSDictionary* info = [NSDictionary dictionaryWithObject:self.node.index forKey:NODE_INDEX_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:EDITED_NODE_NOTIFICATION object:nil userInfo:info];
}
@end
