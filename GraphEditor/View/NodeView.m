//
//  NodeView.m
//  GraphEditor
//
//  Created by Alberto Vaccarella on 28/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import "NodeView.h"

@implementation NodeView
@synthesize model;

-(instancetype)initWithFrame:(CGRect)frame andModel:(GraphNode *)node {
    if(self = [super initWithFrame:frame]) {
        self.model = node;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {

    UIColor* nodeColor = self.model.visited ? [UIColor greenColor] : [UIColor blueColor];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, nodeColor.CGColor);
    CGContextSetFillColorWithColor(context, nodeColor.CGColor);
    
    CGContextBeginPath(context);
    CGContextAddEllipseInRect(context, rect);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    UIGraphicsEndImageContext();
}


@end
