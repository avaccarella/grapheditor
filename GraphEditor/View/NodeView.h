//
//  NodeView.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 28/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphNode.h"

@interface NodeView : UIView
@property GraphNode* model;
-(instancetype)initWithFrame:(CGRect)frame andModel:(GraphNode*)node;
@end
