//
//  GraphTableViewCell.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 29/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphNode.h"

@interface GraphTableViewCell : UITableViewCell
@property(nonatomic) GraphNode* node;

- (IBAction)startExploration:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *adjacentNodesLabel;

@property (strong, nonatomic) IBOutlet UITextField *xValue;
@property (strong, nonatomic) IBOutlet UITextField *yValue;
@property (strong, nonatomic) IBOutlet UITextField *rValue;
@property (strong, nonatomic) IBOutlet UILabel *nodeIndexLabel;

@property (strong, nonatomic) IBOutlet UIButton *exploreButton;

- (IBAction)xEdited:(UITextField *)sender;
- (IBAction)yEdited:(UITextField *)sender;
- (IBAction)radiusEdited:(UITextField *)sender;

@end
