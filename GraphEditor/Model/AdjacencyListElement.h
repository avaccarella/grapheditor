//
//  AdjacencyListElement.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GraphNode.h"

@interface AdjacencyListElement : NSObject

//@property GraphNode* vertex;
/*@property NSUInteger vertexIndex;
@property AdjacencyListElement* nextListElement;
*/

@property NSDictionary* listElementMap;

- (instancetype) initWithIndex:(NSUInteger)index andNodeInfo:(GraphNode*)node;
- (NSUInteger) getNodeIndex;
- (GraphNode*) getNodeInfo;
- (NSArray*)   getAdjacentNodes;
- (void)       addAdjacentNode;

@end
