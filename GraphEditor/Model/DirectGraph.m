//
//  DirectGraph.m
//  GraphEditor
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import "DirectGraph.h"
#import "AdjacencyList.h"
#import "Constants.h"

@implementation DirectGraph

-(instancetype) initWithJsonArray:(NSArray*)nodes {
    
    if(self = [self init]) {
        
        //create graph nodes
        for(NSUInteger i=0; i < nodes.count; ++i) {
            NSDictionary* nodeDict = [nodes objectAtIndex:i];
            double x = [[nodeDict objectForKey:NODE_X_KEY] doubleValue];
            double y = [[nodeDict objectForKey:NODE_Y_KEY] doubleValue];
            double r = [[nodeDict objectForKey:NODE_R_KEY] doubleValue];
            
            GraphNode* node = [[GraphNode alloc] initWithX:x andY:y andRadius:r andIndex:i];
            [self addNode:node];
        }
        
        //create graph arcs
        for(NSUInteger i=0; i < nodes.count; ++i) {

            NSDictionary* nodeDict = [nodes objectAtIndex:i];
            NSArray* adjacentsIndex = [nodeDict objectForKey:NODE_L_KEY];
            
            for(NSNumber* adjIndex in adjacentsIndex) {
                [self addArcFromNode:i toNode:adjIndex.unsignedIntegerValue];
            }
        }
    }
    
    return self;
}

-(instancetype)init {
    if(self = [super init]) {
        self.indexToNodeMap = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

-(void) addArcFromNode:(NSUInteger)srcIndex toNode:(NSUInteger)destIndex {
    
    GraphNode* srcNode = [self.indexToNodeMap objectForKey:@(srcIndex)];
    GraphNode* destNode = [self.indexToNodeMap objectForKey:@(destIndex)];
    
    [srcNode setAdjacent:destNode];
}

//TODO link this logic to UI actions
-(void) removeArcFromNode:(NSUInteger)srcIndex toNode:(NSUInteger)destIndex {
    
    GraphNode* srcNode  = [self.indexToNodeMap objectForKey:@(srcIndex)];
    GraphNode* destNode = [self.indexToNodeMap objectForKey:@(destIndex)];
    
    [srcNode removeFromAdjacents:destNode.index];
}

-(void)addNode:(GraphNode *)newNode {
    [self.indexToNodeMap setObject:newNode forKey:newNode.index];
}

-(void)removeNode:(GraphNode *)nodeToRemove {
    
    NSArray* nodes = [self.indexToNodeMap allValues];
    
    //remove all arcs pointing to nodeToRemove
    for(GraphNode* node in nodes) {
        [node removeFromAdjacents:nodeToRemove.index];
    }
    
    //remove from the graph
    [self.indexToNodeMap removeObjectForKey:nodeToRemove.index];
}


-(void)removeNodeAtIndex:(NSUInteger)nodeToRemoveIdx {
    
    GraphNode* node = [self.indexToNodeMap objectForKey:@(nodeToRemoveIdx)];
    [self removeNode:node];
}

-(NSArray *)getAllOrderedNodeIndexes {
    NSMutableArray* orderedNodeIndexes = [[self.indexToNodeMap allKeys] mutableCopy];
    NSSortDescriptor *ascending = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    [orderedNodeIndexes sortUsingDescriptors:[NSArray arrayWithObject:ascending]];
    return orderedNodeIndexes;
}

-(void)resetVisitedNodes {
    NSArray* nodes = [self.indexToNodeMap allValues];
    for(GraphNode* node in nodes) {
        [node setVisited:NO];
    }
}

//when this recursive implementation of Depth First Search (DFS) ends, all visited graph nodes are marked as such
-(void)computePathsFromNode:(GraphNode *)startingNode {

    [startingNode visit];
    
    NSArray* adjacents = [startingNode getAdjacentNodes];
    for(GraphNode* node in adjacents) {
        if(!node.visited) {
            [self computePathsFromNode:node];
        }
    }
}

@end
