//
//  DirectGraph.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GraphNode.h"

@interface DirectGraph : NSObject

//map: nodes index -> node object
@property(nonatomic, strong) NSMutableDictionary* indexToNodeMap;

-(instancetype) initWithJsonArray:(NSArray*)nodes;
-(void) addArcFromNode:(NSUInteger)srcIndex toNode:(NSUInteger)destIndex;
-(void) removeArcFromNode:(NSUInteger)srcNode toNode:(NSUInteger)destNode;
-(void) addNode:(GraphNode*)newNode;
-(void) removeNode:(GraphNode*)nodeToRemove;
-(void) removeNodeAtIndex:(NSUInteger)nodeToRemoveIdx;
-(NSArray*) getAllOrderedNodeIndexes;
-(void) resetVisitedNodes;

//implement Depth First Search (DFS)
-(void) computePathsFromNode:(GraphNode*)startingNode;

@end