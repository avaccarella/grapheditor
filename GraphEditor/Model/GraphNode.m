//
//  GraphNode.m
//  GraphEditor
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import "GraphNode.h"

@interface GraphNode()
@property NSMutableDictionary* adjacentNodes;//map: nodeIndex -> nodeObject
@end

@implementation GraphNode
@synthesize x;
@synthesize y;
@synthesize r;
@synthesize index;
@synthesize visited;
@synthesize adjacentNodes;

-(instancetype)initWithX:(double)x_
                    andY:(double)y_
               andRadius:(double)r_
                andIndex:(NSUInteger)idx {

    if(self = [super init]) {
        self.x = x_;
        self.y = y_;
        self.r = r_;
        self.index = [[NSNumber alloc] initWithUnsignedInteger:idx];
        self.adjacentNodes = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}


-(void)setAdjacent:(GraphNode *)node {
    [self.adjacentNodes setObject:node forKey:node.index];
}

-(NSArray*)getAdjacentNodes {
    return [self.adjacentNodes allValues];
}

-(NSArray*)getAdjacentNodesIndexes {
    return [self.adjacentNodes allKeys];
}

-(void)removeFromAdjacents:(NSNumber*)nodeIdx {
    [self.adjacentNodes removeObjectForKey:nodeIdx];
}

-(void)visit {
    self.visited = YES;
    NSLog(@"Visiting node: %@", self.index);
}

@end
