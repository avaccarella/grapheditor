//
//  GraphNode.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GraphNode : NSObject

@property double    x;
@property double    y;
@property double    r;
@property BOOL      visited;
@property NSNumber* index;

-(instancetype)initWithX:(double)x_ andY:(double)y_ andRadius:(double)r_ andIndex:(NSUInteger)idx;

//setting an adjacent node means creating an arc
-(void)setAdjacent:(GraphNode*)node;

//removing an adjacent node means deleting an arc
-(void)removeFromAdjacents:(NSNumber*)nodeIdx;

//array of adjacent graph nodes and indexes
-(NSArray*)getAdjacentNodes;
-(NSArray*)getAdjacentNodesIndexes;

//visiting a node means that we walked through it when exploring the graph
//this helps us in listing the linked nodes starting from any vertex in the graph and
//avoids infinite loop in case of cyclic graph
-(void)visit;

@end
