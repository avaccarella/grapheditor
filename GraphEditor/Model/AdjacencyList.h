//
//  AdjacencyList.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdjacencyListElement.h"

@interface AdjacencyList : NSObject
@property AdjacencyListElement* head;
@end
