//
//  AdjacencyListElement.m
//  GraphEditor
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import "AdjacencyListElement.h"

static NSString * const nodeIndexKey            = @"NodeIndex";
static NSString * const nodeInfoKey             = @"NodeInfo";
static NSString * const adjacentNodesArrayKey   = @"AdjacentNodesArray";

@implementation AdjacencyListElement
@synthesize listElementMap;

-(instancetype)initWithIndex:(NSUInteger)index andNodeInfo:(GraphNode *)node {
    if(self = [super init]) {
        
        NSMutableArray* adjacentNodesArray = [NSMutableArray array];
        
        self.listElementMap = [NSDictionary dictionaryWithObjectsAndKeys:
                               @(index), nodeIndexKey,
                               node, nodeInfoKey,
                               adjacentNodesArray, adjacentNodesArrayKey, nil];
    }
    
    return self;
}

-(NSUInteger)getNodeIndex {
    return [[self.listElementMap objectForKey:nodeIndexKey] unsignedIntegerValue];
}

-(GraphNode *)getNodeInfo {
    return [self.listElementMap objectForKey:nodeInfoKey];
}

-(NSArray *)getAdjacentNodes {
    return [self.listElementMap objectForKey:adjacentNodesArrayKey];
}
            
-(void)addAdjacentNode {
    NSMutableArray* adjacentNodes = [self.listElementMap objectForKey:adjacentNodesArrayKey];
}


@end
