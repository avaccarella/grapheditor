//
//  GraphViewController.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 28/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraphViewController : UIViewController<UIAlertViewDelegate>

//0 means create new graph
@property NSUInteger datasetId;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
- (IBAction)backAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *graphContainerView;

@end
