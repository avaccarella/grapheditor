//
//  GraphViewController.m
//  GraphEditor
//
//  Created by Alberto Vaccarella on 28/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import "GraphViewController.h"
#import "GraphOptionsTableViewController.h"
#import "NetworkManager.h"
#import "DirectGraph.h"
#import "NodeView.h"
#import "Constants.h"

@interface GraphViewController ()
@property DirectGraph* graph;
@end

@implementation GraphViewController

@synthesize datasetId;

-(void) viewDidLoad {
    [super viewDidLoad];
    
    [self registerObservers];
    
    if(datasetId > 0) {
        [[NetworkManager getInstance] requestForDataset:datasetId];
    } else if(datasetId == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Coming Soon" message:@"Feature not available" delegate: self cancelButtonTitle: nil otherButtonTitles: @"OK",nil, nil];
        [alert setTag:0];
        [alert show];
    }
}

-(void) registerObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNewGraphData:)
                                                 name:NEW_DATA_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkErrorCallback:)
                                                 name:NETWORK_ERROR_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deletedNodeCallback:)
                                                 name:DELETED_NODE_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(exploreFromNodeCallback:)
                                                 name:EXPLORE_FROM_NODE_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(nodeEditedCallback:)
                                                 name:EDITED_NODE_NOTIFICATION
                                               object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - drawing

-(void) drawGraph {
    
    if(![[NSThread currentThread] isMainThread]) {
        return [self performSelectorOnMainThread:@selector(drawGraph) withObject:nil waitUntilDone:NO];
    }
    
    [self.graphContainerView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];

    //add subview in ascending order so we can easily remove it upon deletion (without looping on datasource)
    NSArray* orderedNodeIndexes = [self.graph getAllOrderedNodeIndexes];

    for(NSNumber* nodeIdx in orderedNodeIndexes) {
        GraphNode* node = [self.graph.indexToNodeMap objectForKey:nodeIdx];
        CGRect frame    = [self frameForNode:node];
        NodeView* nodeView = [[NodeView alloc] initWithFrame:frame andModel:node];
        [nodeView setBackgroundColor:[UIColor clearColor]];
        [self.graphContainerView addSubview:nodeView];
    }
}

-(CGRect) frameForNode:(GraphNode*)node {
    CGSize  screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat rectWidth = 400 * node.r;
    CGFloat x = (node.x - node.r) * 200 + screenSize.width/2;
    CGFloat y = (node.y - node.r) * 200 + screenSize.height/2;
    CGRect frame = CGRectMake(x, y, rectWidth, rectWidth);
    return frame;
}

-(void) redrawNode:(NSNumber*)nodeIdx {
    NodeView* nodeView = [self.graphContainerView.subviews objectAtIndex:nodeIdx.unsignedIntegerValue];
    GraphNode* node = nodeView.model;
    CGRect newFrame = [self frameForNode:node];
    [nodeView setFrame:newFrame];
}

#pragma mark - Navigation

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Pass a reference to the graph object
    GraphOptionsTableViewController* vc = [segue destinationViewController];
    vc.graph = self.graph;
}

#pragma mark - IBActions
-(IBAction) backAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - alert view delegate
-(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 0 ||
       alertView.tag == 1) {
        //create new graph not supported or network error
        //pop back to data selection
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - observers callbacks
-(void) networkErrorCallback:(NSNotification*)aNotification {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network error" message:@"Please check your connection and try again" delegate: self cancelButtonTitle: nil otherButtonTitles: @"OK",nil, nil];
    [alert setTag:1];
    [alert show];
}

-(void) exploreFromNodeCallback:(NSNotification*)aNotification {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary* info = [aNotification userInfo];
        NSNumber* index = [info objectForKey:NODE_INDEX_KEY];
        GraphNode* startingNode = [self.graph.indexToNodeMap objectForKey:index];
        [self.graph resetVisitedNodes];
        [self.graph computePathsFromNode:startingNode];
        [self drawGraph];
    });
}

-(void) deletedNodeCallback:(NSNotification*)aNotification {
    //remove deleted node from view
    NSDictionary* info = [aNotification userInfo];
    NSNumber* index = [info objectForKey:NODE_INDEX_KEY];
    [[self.graphContainerView.subviews objectAtIndex:index.unsignedIntegerValue] removeFromSuperview];
}

-(void) receivedNewGraphData:(NSNotification*)aNotification {
    NSDictionary* userInfo = [aNotification userInfo];
    
    NSData* data = [userInfo objectForKey:DATA_FROM_NETWORK_KEY];
    
    NSArray* nodes = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    if(nodes.count > 0) {
        //handle only valid json files
        self.graph = [[DirectGraph alloc] initWithJsonArray:nodes];
        
        [self performSelectorOnMainThread:@selector(drawGraph) withObject:nil waitUntilDone:NO];
    } else {
        //invalid json, back to dataset picker
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void) nodeEditedCallback:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    NSNumber* index = [info objectForKey:NODE_INDEX_KEY];
    [self redrawNode:index];
}

@end
