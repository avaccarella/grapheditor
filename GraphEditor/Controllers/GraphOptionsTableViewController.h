//
//  GraphOptionsTableViewController.h
//  GraphEditor
//
//  Created by Alberto Vaccarella on 29/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DirectGraph.h"

@interface GraphOptionsTableViewController : UITableViewController
@property DirectGraph* graph;
@end
