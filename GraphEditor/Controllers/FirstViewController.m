//
//  ViewController.m
//  GraphEditor
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import "FirstViewController.h"
#import "DirectGraph.h"
#import "GraphViewController.h"

@interface FirstViewController ()
@property NSDictionary* pickerData;
@end

@implementation FirstViewController

@synthesize pickerData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.pickerData = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"Create new", @0,
                                @"Graph 1" ,   @1,
                                @"Graph 2" ,   @2,
                                @"Graph 3" ,   @3,
                                @"Graph 4" ,   @4,
                                @"Graph 5" ,   @5,
                                @"Graph 6" ,   @6,
                                @"Graph 7" ,   @7,
                                @"Graph 8" ,   @8,
                                @"Graph 9" ,   @9,
                                @"Graph 10",   @10,
                                @"Graph 11",   @11,
                                @"Graph 12",   @12,
                                @"Graph 13",   @13, nil];
    
    self.picker.dataSource  = self;
    self.picker.delegate    = self;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.pickerData.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component {
    
    return [self.pickerData objectForKey:@(row)];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    //pass user selection to next controller
    NSUInteger pickerIndex  = [self.picker selectedRowInComponent:0];
    GraphViewController* vc = [segue destinationViewController];
    vc.datasetId = pickerIndex;
}

@end
