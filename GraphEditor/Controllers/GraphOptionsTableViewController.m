//
//  GraphOptionsTableViewController.m
//  GraphEditor
//
//  Created by Alberto Vaccarella on 29/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import "GraphOptionsTableViewController.h"
#import "GraphTableViewCell.h"
#import "Constants.h"

@interface GraphOptionsTableViewController ()

@end

static NSString * const NodeOptionsCellReuseIdentifier = @"nodeoptionscell";

@implementation GraphOptionsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINib *nib = [UINib nibWithNibName:@"GraphTableCell"
                                bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:nib forCellReuseIdentifier:NodeOptionsCellReuseIdentifier];
    [self.tableView setAllowsSelection:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.graph.indexToNodeMap.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    GraphTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NodeOptionsCellReuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSArray* orderedNodeIndexes = [self.graph getAllOrderedNodeIndexes];
    NSNumber* nodeIndexForRow = [orderedNodeIndexes objectAtIndex:indexPath.row];
    
    GraphNode* node = [self.graph.indexToNodeMap objectForKey:nodeIndexForRow];
    cell.node = node;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170.0;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        GraphTableViewCell* cell = (GraphTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        NSUInteger nodeIdx = cell.node.index.unsignedIntegerValue;
        [self.graph removeNodeAtIndex:nodeIdx];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        NSDictionary* info = [NSDictionary dictionaryWithObject:@(nodeIdx) forKey:NODE_INDEX_KEY];
        [[NSNotificationCenter defaultCenter] postNotificationName:DELETED_NODE_NOTIFICATION object:nil userInfo:info];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

@end
