//
//  FMNetworkManager.h
//  flickrmap
//
//
//

#import <Foundation/Foundation.h>


//NSString* const urlFormat;
//NSString* const dataNotificationName;
#define urlFormat  @"http://zip.paginebianche.it/test/%@.json"

@interface NetworkManager : NSObject<NSURLSessionDelegate>
+ (NetworkManager*) getInstance;
-(void)requestForDataset:(NSUInteger)datasetId;
@property NSURLSession* nsUrlSession;

@end
