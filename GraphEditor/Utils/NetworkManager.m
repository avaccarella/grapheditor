//
//  FMNetworkManager.m
//  flickrmap
//
//
//

#import "NetworkManager.h"
#import "Constants.h"


static NetworkManager* instance = nil;

@implementation NetworkManager
@synthesize nsUrlSession;

+ (NetworkManager*)getInstance {
    if (instance == nil) {
        instance = [[NetworkManager alloc] init];
    }
    return instance;
}

- (id) init {
    if (self = [super init]) {
        
        NSURLSessionConfiguration* defaultSession = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        self.nsUrlSession = [NSURLSession sessionWithConfiguration:defaultSession delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    }
    return self;
}


-(void)requestForDataset:(NSUInteger)datasetId {

    NSString* dataset = [self createDatasetName:datasetId];
    NSString* urlStr  = [NSString stringWithFormat:urlFormat, dataset];
    NSURL*    url     = [NSURL URLWithString:urlStr];
    
    NSURLSessionDataTask* task = [self.nsUrlSession dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NETWORK_ERROR_NOTIFICATION object:nil userInfo:nil];
            NSLog(@"Network error: %@", error.description);
        } else {
            NSDictionary* userInfo = [NSDictionary dictionaryWithObject:data forKey:DATA_FROM_NETWORK_KEY];
            [[NSNotificationCenter defaultCenter] postNotificationName:NEW_DATA_NOTIFICATION object:nil userInfo:userInfo];
        }

    }];
    [task resume];
}

#pragma mark -
#pragma mark utility methods

- (NSString*) createDatasetName:(NSUInteger)datasetId {
    if(datasetId > 9) {
        return [NSString stringWithFormat:@"Graph%lu", datasetId];
    } else {
        return [NSString stringWithFormat:@"Graph0%lu", datasetId];
    }
}

@end
