//
//  GraphEditorTests.m
//  GraphEditorTests
//
//  Created by Alberto Vaccarella on 27/05/15.
//  Copyright (c) 2015 Alberto Vaccarella. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "DirectGraph.h"
#import "Constants.h"

@interface GraphEditorTests : XCTestCase

@end

@implementation GraphEditorTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testNodeHasProperties {
    
    NSArray* testSet = [self getTestNodesInfo:3];
    for (NSDictionary* nodeinfo in testSet) {
        
        double x = [[nodeinfo objectForKey:NODE_X_KEY] doubleValue];
        double y = [[nodeinfo objectForKey:NODE_Y_KEY] doubleValue];
        double r = [[nodeinfo objectForKey:NODE_R_KEY] doubleValue];
        double idx = [[nodeinfo objectForKey:NODE_INDEX_KEY] unsignedIntegerValue];
        
        GraphNode* node = [[GraphNode alloc] initWithX:x
                                                  andY:y
                                             andRadius:r
                                              andIndex:idx];
        
        BOOL success = (node.x == x &&
                        node.y == y &&
                        node.r == r &&
                        node.index.unsignedIntegerValue == idx);
        
        XCTAssert(success);
    }
}

-(void) testAddingArcsBetweenNodes {

    //create a graph with 5 nodes
    DirectGraph* graph = [self createTestGraphWithNodes:5];

    NSMutableDictionary* adjacenciesForNodeMap = [[NSMutableDictionary alloc] init];

    //node 0 has a link towards node 1 and 2, and so on....
    [adjacenciesForNodeMap setObject:@[@1,@2] forKey:@0];
    [adjacenciesForNodeMap setObject:@[@3] forKey:@1];
    [adjacenciesForNodeMap setObject:@[@0,@1,@4] forKey:@2];
    [adjacenciesForNodeMap setObject:@[@2,@4] forKey:@3];
    [adjacenciesForNodeMap setObject:@[@1] forKey:@4];
    
    //adding arcs
    NSArray* startingNodes = [adjacenciesForNodeMap allKeys];
    for(NSNumber* startingNode in startingNodes) {
        NSArray* destNodes = [adjacenciesForNodeMap objectForKey:startingNode];
        for (NSNumber* destNode in destNodes) {
            [graph addArcFromNode:startingNode.unsignedIntegerValue toNode:destNode.unsignedIntegerValue];
        }
    }
    
    //verify each node has correct adjacent nodes
    for(NSNumber* startingNode in startingNodes) {
        GraphNode* node = [graph.indexToNodeMap objectForKey:startingNode];
        NSArray* adj = [node getAdjacentNodesIndexes];
        NSArray* destNodes = [adjacenciesForNodeMap objectForKey:startingNode];
        
        BOOL success = [adj isEqualToArray:destNodes];
        XCTAssert(success);
    }
}

- (void)testExploreGraphDFS {
    //create a graph with 5 nodes
    DirectGraph* graph = [self createTestGraphWithNodes:5];
    
    NSMutableDictionary* adjacenciesForNodeMap = [[NSMutableDictionary alloc] init];
    
    //node 0 has a link towards node 1 and 2, and so on....
    [adjacenciesForNodeMap setObject:@[@1,@2] forKey:@0];
    [adjacenciesForNodeMap setObject:@[@3] forKey:@1];
    [adjacenciesForNodeMap setObject:@[@0,@1,@4] forKey:@2];
    [adjacenciesForNodeMap setObject:@[@2,@4] forKey:@3];

    NSMutableDictionary* visitedFromNodeMap = [[NSMutableDictionary alloc] init]; //starting node -> all possibly linked nodes
    [visitedFromNodeMap setObject:@[@0,@1,@2,@3,@4] forKey:@0];
    [visitedFromNodeMap setObject:@[@0,@1,@2,@3,@4] forKey:@1];
    [visitedFromNodeMap setObject:@[@0,@1,@2,@3,@4] forKey:@2];
    [visitedFromNodeMap setObject:@[@0,@1,@2,@3,@4] forKey:@3];
    [visitedFromNodeMap setObject:@[@4] forKey:@4];
    
    //adding arcs
    NSArray* startingNodes = [adjacenciesForNodeMap allKeys];
    for(NSNumber* startingNode in startingNodes) {
        NSArray* destNodes = [adjacenciesForNodeMap objectForKey:startingNode];
        for (NSNumber* destNode in destNodes) {
            [graph addArcFromNode:startingNode.unsignedIntegerValue toNode:destNode.unsignedIntegerValue];
        }
    }
    
    for(NSNumber* startingNode in startingNodes) {
        GraphNode* node = [graph.indexToNodeMap objectForKey:startingNode];
        [graph computePathsFromNode:node];
        NSArray* computed = [self getAllVisitedNodesInGraph:graph];
        NSArray* desired  = [visitedFromNodeMap objectForKey:node.index];
        BOOL success = [computed isEqualToArray:desired];
        XCTAssert(success);
        [graph resetVisitedNodes];
    }
    
}

#pragma mark - utilities

-(NSArray*)getTestNodesInfo:(NSUInteger)datasetLength {
    NSMutableArray* arr = [[NSMutableArray alloc] initWithCapacity:datasetLength];
    for(NSUInteger i = 0; i < datasetLength; ++i) {
        double x = drand48();
        double y = drand48();
        double r = drand48();
        NSUInteger idx = arc4random();
        NSDictionary* nodeDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  @(x), NODE_X_KEY,
                                  @(y), NODE_Y_KEY,
                                  @(r), NODE_R_KEY,
                                  @(idx), NODE_INDEX_KEY,nil];
        [arr addObject:nodeDict];
    }
    return arr;
}

-(NSArray*) getAllVisitedNodesInGraph:(DirectGraph*)graph {
    
    NSMutableArray* visited = [[NSMutableArray alloc] init];
    NSArray* nodes = [graph.indexToNodeMap allValues];
    
    for(GraphNode* node in nodes) {
        if(node.visited) {
            [visited addObject:node.index];
        }
    }
    
    //ascending order for later comparison
    NSSortDescriptor *ascending = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    [visited sortUsingDescriptors:[NSArray arrayWithObject:ascending]];
    
    return visited;
}

- (DirectGraph*)createTestGraphWithNodes:(NSUInteger)numberOfNodes {

    DirectGraph* graph = [[DirectGraph alloc] init];

    for(NSUInteger i = 0; i < numberOfNodes; ++i) {
        GraphNode* node = [[GraphNode alloc] initWithX:0.0 andY:0.0 andRadius:0.0 andIndex:i];
        [graph addNode:node];
    }
    
    return graph;
}

@end
